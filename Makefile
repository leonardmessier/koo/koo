.DEFAULT_GLOBAL := all

install:
	@npm install

build:
	@tsc

package:
	@cp -r node_modules build/node_modules
	@cd build && tar czf ../koo.tar.gz * --transform 's,^,koo/,' && cd - >/dev/null

all: build

.PHONY: install build package
