#!/usr/bin/env node

const prog = require('caporal');
const fs = require('fs');
const _ = require('lodash');
const glob = require('glob');
const path = require('path');
const { spawn } = require('child_process');
const mustache = require('mustache');
const prmpt = require('prompt');


const index = function(logger, cb) {
  const env = process.env
  const defaults = {};
  const configFile = fs.readFileSync(`${env.XDG_CONFIG_HOME}/koo/config`, { encoding: 'UTF-8' })
  let config = _.extend(defaults, JSON.parse(configFile))

  let promises = []
  config.directories.forEach((element) => {
    promises.push(findProjects(element));
  })

  Promise.all(promises)
    .then((values) => {
      let projects = [];
      const indexFile = `${env.HOME}/.cache/koo/index`;
      values = _.flattenDeep(values);
      values.forEach(function(project) {
        if (_.isEmpty(project)) {
          return;
        }

        let typeChar = 'ﮛ';
        switch(project.config.type) {
            case 'drupal':
                typeChar = '';
                break;
            case 'vim':
                typeChar = '';
                break;
            case 'docker':
                typeChar = '';
                break;
            case 'spec':
                typeChar = 'ﴬ';
                break;
        }

        projects.push(`${typeChar} - ${project.config.name} - ${project.config.client} - ${project.path}`);
      });

      if (projects.length) {
        fs.writeFile(indexFile, projects.join("\n"), 'utf8', (err) => {
          cb();
        });
      }
    })
    .catch((err) => {
      console.log(err);
    })
  ;

};

const readKoorcFile = function(file) {
  const fullPath = path.dirname(file);
  return new Promise(function(resolve, reject) {
    fs.readFile(file, 'utf8', function(err, data) {
      if (err) {
        return reject(err);
      }

      const koorc = JSON.parse(data);
      return resolve({
        path: fullPath,
        config: koorc
      });
    });
  });
}

const findProjects = function(directory) {
  return new Promise(function(resolve, reject) {
    const koorcPath = directory + '/koorc.json';
    const results = [];
    glob(koorcPath, {dot: true}, function(err, files) {
      if (err) {
          return reject(err);
      }

      if (!files.length) {
          return resolve({});
      }

      const fPromises = []
      files.forEach(function(f) {
        fPromises.push(readKoorcFile(f));
      });

      Promise.all(fPromises)
        .then((values) => {
          return resolve(values);
        })
        .catch((err) => {
          return reject(err);
        })
      ;
    });
  });
};

const listOld = function listOld(logger, cb) {
  const env = process.env;

  const indexFile = `${env.HOME}/.cache/koo/index`;
  fs.exists(indexFile, (exists) => {
    if (!exists) {
      index(logger, function() {
        fs.readFile(indexFile, 'utf8', (err, data) => {
          cb(data);
        });
      });
    } else {
        fs.readFile(indexFile, 'utf8', (err, data) => {
          cb(data);
        });
    }
  });
}

const list = function list(logger) {
  const env = process.env;
  let promise = new Promise((resolve, reject) => {
    fs.readFile(`${env.HOME}/.cache/koo/index`, 'utf8', (err, data) => {
        if (err) {
            return reject(err);
        }

        return resolve(data);
    });
  });

  return promise;
};

const projectActions = function(path) {
  const env = process.env;
  let promise = new Promise((resolve, reject) => {
    fs.readFile(`${path}/koorc.json`, 'utf8', (err, data) => {
        if (err) {
            return reject(err);
        }
        const json = JSON.parse(data);
        const defaultActions = [
            'List windows',
            'Edit koorc'
        ];
        const tags = _.uniq(_.flattenDeep(_.map(json.windows, 'tags')));

        const actions = _.concat(defaultActions, tags);

        return resolve(actions);
    });
  });

  return promise;

};

/*const readKoorcFile = function readKoorcFile(path) {*/
  //return new Promise((resolve, reject) => {

      //fs.readFile(`${path}/koorc.json`, 'utf8', (err, data) => {
          //if (err) {
              //return reject(err);
          //}
          //const json = JSON.parse(data);

          //return resolve(json);
      //});
  //});
//}

const getCommands = function getCommands(action, path) {
 const env = process.env;
  let promise = new Promise((resolve, reject) => {

    fs.readFile(`${path}/koorc.json`, 'utf8', (err, data) => {
        if (err) {
            return reject(err);
        }
        const json = JSON.parse(data);

        const windows = _.filter(json.windows, (w) => {
          return w.tags.includes(action);
        });
        const commands = _.map(windows, 'command');

        return resolve(commands);
    });

  });

  return promise;
}

const launchProject = function launchProject() {

}

const listWindows = function listWindows() {

}

const openKoorc = function openKoorc (projectPath) {
  return new Promise((resolve, reject) => {
      readKoorcFile(projectPath)
        .then((config) => {
          return resolve([
            {
              command: 'edit-file',
              args: [ config.name, `${projectPath}/koorc.json` ]
            }
          ])
        })
        .catch((err) => {
          return reject(err);
        })
      ;
  });
}

const route = function route(action, projectPath) {
  let a = action.toLowerCase();
  switch (a) {
    case 'open project':
        return getCommands(action, projectPath);
        break;
    case 'launch project':
        return getCommands(action, projectPath);
        break;
    case 'edit koorc':
        return openKoorc(projectPath);
        break;
    default:
      throw new Error(`Unhandled action ${a}`);
  }
}

const executeCommand = function(command, args, data) {
  let promise = new Promise((resolve, reject) => {
    let child = spawn(command, args);

    if (typeof data !== 'undefined') {
        child.stdin.write(data);
    }
    child.stdout.on('data', function(stdout) {
      return resolve(stdout.toString().trim());
    });

    child.stderr.on('data', function(stderr) {
      return reject(stderr);
    });

    child.stdin.end();
  });

  return promise;

}

const render = function(data) {
  const env = process.env;
  let promise = new Promise((resolve, reject) => {
    let child = spawn('docker', [
      'run',
      '-i',
      '-e',
      `DISPLAY=${env.DISPLAY}`,
      '-v',
      '/tmp/.X11-unix:/tmp/.X11-unix',
      '-v',
      '/home/leonard/.ssh:/home/dev/.ssh',
      '-v',
      '/home/leonard/.config/:/home/dev/.config',
      '-v',
      '/home/leonard/.fonts:/home/dev/.fonts',
      'captainquirk/docker-rofi:1.5.0',
      '-dmenu',
      '-theme',
      'oxide',
      '-p',
      ''
    ]);

    child.stdin.write(data);
    child.stdout.on('data', function(stdout) {
      return resolve(stdout.toString().trim());
    });

    child.stderr.on('data', function(stderr) {
      return reject(stderr);
    });

    child.stdin.end();
  });

  return promise;
};

const makeConfigDir = function makeConfigDir(name) {
  let promise = new Promise((resolve, reject) => {
    fs.mkdir(`${name}/.config`, function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();

    });
  });

  return promise;
};

const makeProjectDir = function makeProjectDir(name) {
  let promise = new Promise((resolve, reject) => {
    fs.mkdir(name, function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();

    });
  });

  return promise;
};


const makeCodeDir = function makeConfigDir(name) {
  let promise = new Promise((resolve, reject) => {
    fs.mkdir(`${name}/Code`, function(err) {
      if (err) {
        return reject(err);
      }

      return resolve();

    });
  });

  return promise;
};

const promptProjectName = function() {
  let promise = new Promise((resolve, reject) => {
      prmpt.start();

      prmpt.get(['name'], function(err, result) {
        if (err) {
            return reject(err);
        }
        return resolve(result.name);
      });

    });

  return promise;

}

const writeKooRcFile = function writeKooRcFile() {
  let promise = new Promise((resolve, reject) => {
    fs.readFile(__dirname + '/templates/' + koorc.json.mustache, { encoding: 'UTF-8' }, function (err, data) {
        const output = mustache.render(data);
    });
    return resolve();
  });

  return promise;
}

const writeTmuxinatorOpenFile = function writeTmuxinatorOpenFile() {
  let promise = new Promise((resolve, reject) => {
    return resolve();
  });

  return promise;
}

const init = function init(logger) {
  const projectName = '';
  promptProjectName()
    .then((name) => {
      projectName = name;
      return makeProjectDir(projectName);
    })
    .then(() => {
      return makeConfigDir(projectName);
    })

    .then(() => {
      return makeCodeDir(projectName);
    })
    .then(() => {
      process.exit(0);
      return writeKooRcFile();
    })
    .then((values) => {
      return writeTmuxinatorOpenFile();
    })
    .then((values) => {
      index(logger, () => {
      });
    })
 ;

};

let projectPath = '';
let projectName = '';

prog
    .action((args, options, logger) => {
      list(logger)
        .then((value) => {
          return render(value);
        })
        .catch((err) => {
            console.log(err);
        })
        .then((value) => {
          ([, projectName,, projectPath] = value.split(' - '));
          let path = projectPath.trim();

          return projectActions(path);
        })
        .catch((err) => {
            console.log(err);
        })
        .then((value) => {
          return render(value.join("\n"));
        })
        .then((value) => {
          return route(value, projectPath);
        })
        .then((value) => {
          let promises = []
          value.forEach(function(cmd) {
              promises.push(executeCommand(cmd.name, cmd.args));
          });
          Promise.all(promises)
          .then((values) => {
            console.log(values.toString());
          })
          .catch((err) => {
            console.log(err.toString());
          })
          ;
        })
    })
    .command('list', 'List projects')
    .action((args, options, logger) => {
      list(logger, function(data) {
        console.log(data);
      });
    })
    .alias('ls')
    .command('index', 'Index projects')
    .action((args, options, logger) => {
        index(logger, function() {
          //console.log('index generated');
        });
    })
    .command('init', 'Init project')
    .option('--name <project>', 'Project name')
    .action((args, options, logger) => {
       init(logger);
    })
;

prog.parse(process.argv);
