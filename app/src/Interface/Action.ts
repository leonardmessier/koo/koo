import {Project} from '../Entity/Project'

export interface Action {
    title: string

    run(project: Project): any
}
