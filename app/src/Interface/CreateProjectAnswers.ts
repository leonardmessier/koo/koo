import {Answers} from 'inquirer';
import {ProjectType} from './../Enum/ProjectType'
import {CreateProjectAction} from './../Enum/CreateProjectAction'

export interface CreateProjectAnswers extends Answers {
    projectName: string
    client: string
    type: ProjectType,
    action: CreateProjectAction,
    getPassword: boolean,
    pass: string,
    repository: string
}
