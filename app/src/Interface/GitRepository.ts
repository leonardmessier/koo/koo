import {ProjectConfiguration} from './../Entity/ProjectConfiguration'

export interface GitRepository {
    path: string,
    configuration: ProjectConfiguration
}
