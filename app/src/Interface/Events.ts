import {Action} from './Action'

export interface Events {
    [x: string]: Action
}
