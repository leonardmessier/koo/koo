export interface Config {
    directories: string[],
    baseDirectory: string,
    index: string
}
