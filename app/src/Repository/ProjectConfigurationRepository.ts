import {injectable, inject} from "inversify";
import {ProjectConfiguration} from './../Entity/ProjectConfiguration'
import {Project} from './../Entity/Project'
import * as fs from 'fs'
import * as path from 'path'
import {Deserialize,Serialize} from 'cerialize'
import {FileSystem} from '../Service/FileSystem'
import {Config} from '../Interface/Config'
import {GitRepository} from '../Interface/GitRepository'
import {PathBuilder} from '../Service/PathBuilder'
import {RawJsonSerializer} from '../Serializer/RawJsonSerializer'
import _ from 'lodash'

@injectable()
export class ProjectConfigurationRepository {
    public static JSON_SPACE = 2

    private fileSystem: FileSystem
    private config: Config
    private pathBuilder: PathBuilder

    public constructor(
        @inject(Symbol.for("FileSystem")) fileSystem: FileSystem,
        @inject(Symbol.for("Config")) config: Config,
        @inject(Symbol.for("PathBuilder")) pathBuilder: PathBuilder
    ) {
        this.fileSystem = fileSystem
        this.config = config
        this.pathBuilder = pathBuilder
    }

    public async findByProject(project: Project): Promise<ProjectConfiguration> {
        try {
            const projectConfiguration: string = await this.promiseToRead(project)
            return RawJsonSerializer.deserialize(projectConfiguration, ProjectConfiguration)
        } catch (e) {
            throw new Error('No project Configuration found')
            console.error(e)
        }
    }

    public async findAll() {
        let projectConfigurations: ProjectConfiguration[]
        let promises: Promise<GitRepository[]>[] = []
        this.config.directories.forEach((element: any) => {
            promises.push(
                this.fileSystem.find(element, 'koorc.json')
            );
        })

        return new Promise((resolve, reject) => {
            Promise.all(promises)
                .then(function(values) {
                    let configurations: ProjectConfiguration[] = []
                    _.flattenDeep<GitRepository>(values).forEach((gitRepository: GitRepository) => {
                        let projectConfigurationEntity = Deserialize(gitRepository.configuration, ProjectConfiguration)
                        projectConfigurationEntity.path = gitRepository.path
                        configurations.push(projectConfigurationEntity)
                    })
                    resolve(configurations);
                })
        })
    }

    public async save(projectConfiguration: ProjectConfiguration) {
        const path = `${this.pathBuilder.projectFromConfiguration(projectConfiguration)}/.config/koorc.json`;
        const content = this.serializeToString(projectConfiguration)
        try {
            await this.fileSystem.write(path, content)
        } catch (e) {
            console.error(e)
            return process.exit(1)
        }
    }

    private serializeToString(projectConfiguration: ProjectConfiguration) {
        return RawJsonSerializer.serialize(projectConfiguration, ProjectConfiguration)
    }

    private async promiseToRead(project: Project): Promise<string> {
        return new Promise((resolve, reject) => {
            fs.readFile(`${project.path}/koorc.json`, 'utf8', (err, data) => {
                if (err) {
                    return reject(err)
                }

                resolve(data)
            });
        });
    }
}

