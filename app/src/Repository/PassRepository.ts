import {injectable} from 'inversify'
import * as fs from 'fs'
import * as path from 'path'
import * as os from 'os'

@injectable()
export class PassRepository {
    public static readonly BASE_PASS_DIR: string = os.homedir() + '/.password-store'

    public findAll() {
        return this._walkSync(PassRepository.BASE_PASS_DIR)
    }

    private _walkSync(dir: string, filelist?: string[]) {
        let files = fs.readdirSync(dir);
        const that = this;

        filelist = filelist || [];
        files.forEach(function(file) {
            if (file.match(/\.git/)) {
                return;
            }

            if (file === '.gpg-id') {
                return;
            }
            if (fs.statSync(path.join(dir, file)).isDirectory()) {
                filelist = that._walkSync(path.join(dir, file), filelist);
            } else {
                if (typeof filelist !== 'undefined') {
                    filelist.push(path.join(dir, file).replace('.gpg', '').replace(`${PassRepository.BASE_PASS_DIR}/`, ''));
                }
            }

        });

        return filelist;
    }

}
