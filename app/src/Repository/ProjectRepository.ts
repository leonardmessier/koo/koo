import {injectable, inject} from "inversify";
import * as fs from 'fs'
import * as path from 'path'
import {Project} from './../Entity/Project'
import {ProjectSerializer} from './../Serializer/ProjectSerializer'
import {FileSystem} from '../Service/FileSystem'
import {Config} from '../Interface/Config'

@injectable()
export class ProjectRepository {
    private indexPath: string = '/home/leonard/.cache/koo/index'
    private fileSystem: FileSystem
    private config: Config

    public constructor(
        @inject(Symbol.for("FileSystem")) fileSystem: FileSystem,
        @inject(Symbol.for("Config")) config: Config
    ) {
        this.fileSystem = fileSystem
        this.config = config
    }

    public async findAll(): Promise<Project[]> {
        try {
            const projects = await this.promiseToRead()
            return projects;
        } catch (e) {
            console.error(e)
            return [];
        }
    }

    public async saveAll(projects: Project[]) {
        this.writeToIndex(projects)
    }

    private async promiseToRead(): Promise<Project[]> {
        if (this.fileSystem.existsSync(this.getIndexPath())) {
            return new Promise(this.readFromIndex.bind(this));
        } else {
            return new Promise(function(){});
        }
    }

    private readFromIndex(resolve: Function, reject: Function) {
        fs.readFile(this.getIndexPath(), 'utf8', (err, data) => {
            if (err) {
                return reject(err);
            }

            let result: Project[] = [];
            data.split("\n").forEach(function(line) {
                if (line !== "") {
                    result.push(ProjectSerializer.deserialize(line))
                }
            });
            return resolve(result);
        });
    }

    private async writeToIndex(projects: Project[]) {
        let lines: string[] = []
        projects.forEach((project) => {
            lines.push(ProjectSerializer.serialize(project))
        })

        await this.fileSystem.write(this.getIndexPath(), lines.join("\n"))
    }

    private getIndexPath() {
        return path.resolve(this.config.index)
    }
}
