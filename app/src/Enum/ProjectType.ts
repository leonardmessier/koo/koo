export enum ProjectType {
    Drupal = 'drupal',
    Vim = 'vim',
    Docker = 'docker',
    Spec = 'spec'
}
