export enum ProjectTypeChar {
    drupal = '',
    vim = '',
    docker = '',
    spec = 'ﴬ',
    other = 'ﮛ'
}
