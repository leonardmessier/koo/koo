export enum CreateProjectAction {
    CREATE = 'Create new project',
    IMPORT = 'Import existing project'
}
