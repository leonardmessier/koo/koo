import { spawn } from 'child_process'
import {KeysIndex} from './../../Interface/KeysIndex'
import {Events} from './../../Interface/Events'
import {Project} from './../../Entity/Project'
import {ProjectSerializer} from './../../Serializer/ProjectSerializer'
import * as _ from 'lodash'

export class RofiMenu {
    private rofiCommand: string = 'rofi';
    private rofiCommandArgs: string[] = [
        '-dmenu'
    ]

    private rofiPrompt: string = '';

    protected events: Events = {};

    protected items: any[];
    private keysIndex: KeysIndex = {};

    constructor(items: any[]) {
        this.items = items;
    }

    public async render() {
        const data: string = this.items
            .map((item) => { return ProjectSerializer.serialize(item)})
            .join("\n")
        this.renderMenu(data);
    }

    public on(events: Events) {
        this.events = events;
        this.indexCustomKeys();
    }

    protected renderMenu(data: string) {
        const that = this;
            let rofiArgs: string[] = that.rofiCommandArgs;
            this.addRofiPrompt(rofiArgs);
            this.addCustomKeys(rofiArgs);
            this.addCustomKeysMesg(rofiArgs);

            let child = spawn(that.rofiCommand, rofiArgs);
            child.stdin.write(data);
            child.stdout.on('data', function(stdout) {
                child.on('close', function(code) {
                    let sIndex = (code - 9).toString()
                    let keyFromCode: string = that.keysIndex[sIndex];
                  console.log(keyFromCode)
                  console.log(that.events)
                    let callback = that.events[keyFromCode].run(ProjectSerializer.deserialize(stdout.toString().trim()));
                });
            });

            child.stderr.on('data', function(stderr) {
                console.error(stderr);
                throw new Error(stderr)
            });

            child.stdin.end();
    }

    private addRofiPrompt(rofiArgs: string[]) {
        rofiArgs.push('-p', this.rofiPrompt);
    }

    private addCustomKeys(rofiArgs: string[]) {
        let index: string;
        for (index in this.keysIndex) {
            rofiArgs.push(`-kb-custom-${index}`, this.keysIndex[index]);
        }
    }

    private addCustomKeysMesg(rofiArgs: string[]) {
      const lines: string[] = []
        _.each(this.events, function(element, key) {
            lines.push(`<span foreground="#cb4b16"><b>${key}</b></span> : <i>${element.title}</i>`)
        })

        rofiArgs.push('-mesg', lines.join("\n"))
    }

    private indexCustomKeys() {
        let key: string;
        let index: number = 1;
        for (key in this.events) {
            if (!this.events.hasOwnProperty(key)) {
                continue;
            }

            this.keysIndex[index.toString()] = key;
            index++;
        }
    }
}
