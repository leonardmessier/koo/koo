import { deserialize, serialize } from 'cerialize'

import { Command } from './../Interface/Command'
export class Window {
    @deserialize
    @serialize
    name: string = ''

    @deserialize
    @serialize
    command: Command

    @serialize
    @deserialize
    tags: string[] = []

    public constructor (name: string, command: Command, tags: string[]) {
        this.name = name
        this.command = command
        this.tags = tags
    }
}
