import { deserialize, serialize } from 'cerialize'
import { Window } from './Window'
import { ProjectType } from './../Enum/ProjectType'

export class ProjectConfiguration {
    private _path: string = ''

    @deserialize
    @serialize
    name: string = ''

    @deserialize
    @serialize
    client: string = ''

    @deserialize
    @serialize
    type: string = ''

    @deserialize
    @serialize
    layout: string = ''

    @deserialize
    @serialize
    windows: Window[] = []

    public constructor(name: string, client: string, type: string) {
        this.name = name
        this.client = client
        this.type = type
        this.layout = 'full'

        this.windows.push(new Window("Vim", { name: "open-project" }, [ "Open project", "Launch project" ]))
    }

    get path(): string {
        return this._path
    }

    set path(path: string) {
        this._path = path
    }

    public getCommandsByTag(tag: string) {
      return this.windows
          .filter((window) => {
              return window.tags.includes(tag)
          })
          .map((window) => {
              return window.command.name
          })
    }
}
