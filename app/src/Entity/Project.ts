import { deserialize } from 'cerialize'
import { Window } from './Window'

export class Project {
    private _typeChar: string = ''
    private _path: string = ''
    private _name: string = ''
    private _client: string = ''

    get typeChar() {
        return this._typeChar
    }
    set typeChar(typeChar: string) {
        this._typeChar = typeChar
    }

    get path() {
        return this._path
    }
    set path(path: string) {
        this._path = path
    }

    get name() {
        return this._name
    }
    set name(name: string) {
        this._name = name
    }

    get client() {
        return this._client
    }
    set client(client: string) {
        this._client = client
    }
}

