import {injectable, inject} from 'inversify'
import * as exec from 'child_process'
import {PassRepository} from './../Repository/PassRepository'

@injectable()
export class PassService {
    private passRepository: PassRepository

    public constructor(
        @inject(Symbol.for("PassRepository")) passRepository: PassRepository
    ) {
        this.passRepository = passRepository
    }

    public copyToClipboard(passwordName: string) {
        let passCommand = `pass -c ${passwordName}`

        exec.execSync(passCommand);
    }

    public getAll() {
        return this.passRepository.findAll()
    }
}
