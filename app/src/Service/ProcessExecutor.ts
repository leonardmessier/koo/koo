import { spawn } from 'child_process'
import {injectable} from "inversify";
import {Project} from './../Entity/Project'

@injectable()
export class ProcessExecutor {
    public execute(commands: string[], project: Project): Promise<any> {
        const promises: Promise<string>[] = []
        commands.forEach((c) => {
            promises.push(this.executeCommand(c, [ project.name, project.path ]))
        })

        return Promise.all(promises)
    }

    private executeCommand(command: any, args: string[], data?: any): Promise<any> {
        let promise = new Promise((resolve, reject) => {
            let child = spawn(command, args)

            if (typeof data !== 'undefined') {
                child.stdin.write(data)
            }
            child.stdout.on('data', function(stdout) {
                resolve(stdout.toString().trim())
            });

            child.stderr.on('data', function(stderr) {
                reject(stderr)
            });

            child.stdin.end()
        });

        return promise
    }

}

