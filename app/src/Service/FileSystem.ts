import * as fs from 'fs'
import glob from 'glob'
import * as path from 'path'
import {injectable} from 'inversify'
import {GitRepository} from './../Interface/GitRepository'

@injectable()
export class FileSystem {
    public existsSync(path: string) {
        return fs.existsSync(path)
    }

    public read(path: string) {
        return fs.readFileSync(path, 'utf8')
    }

    public async write(filePath: string, content: string) {
        await this.createDirIfNotExist(path.dirname(filePath))
        return new Promise(function(resolve, reject) {
            fs.writeFile(filePath, content, 'utf8', (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve();
                }
            });
        })
    }

    public async find(directory: string, file: string): Promise<GitRepository[]> {
        const koorcPath = directory + '/koorc.json';
        const me = this
        return new Promise(function(resolve, reject) {
            const results = [];
            glob(koorcPath, {dot: true}, function(err, files) {
                if (err) {
                    return reject(err);
                }

                const fPromises: any = []
                files.forEach(function(f) {
                    fPromises.push({
                        path: path.dirname(f),
                        configuration: JSON.parse(me.read(f))
                    });
                });

                return resolve(fPromises);
            });

        });
    }

    public async createDirIfNotExist(directory: string) {
        let dirExists: boolean
        try {
            dirExists = await this.dirExists(directory)
        } catch (e) {
            dirExists = false
        }

        if (!dirExists) {
            fs.mkdirSync(directory, { recursive: true })
        }
    }

    public async dirExists(directory: string): Promise<boolean> {
        return new Promise((resolve: Function, reject: Function) => {
            fs.readdir(directory, function(err) {
                if (err) {
                    return reject(err)
                }
                resolve(true)
            })
        })
    }

    public async fileExists(path: string): Promise<boolean> {
        return new Promise((resolve: Function, reject: Function) => {
            fs.readFile(path, function(err) {
                if (err) {
                    return resolve(false)
                }
                return resolve(true)
            })
        })
    }


    public async symlink(target: string, path: string) {
        return new Promise((resolve: Function, reject: Function) => {
            fs.symlink(target, path, function(err) {
                if (err) {
                    return reject(err)
                }

                return resolve()
            })
        })

    }
}
