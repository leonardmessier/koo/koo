import {injectable} from 'inversify'
import * as exec from 'child_process'

@injectable()
export class GitService {
    public clone(url: string, directory: string) {
        let gitCommand = `git clone ${url} ${directory}/Code`;
        exec.execSync(gitCommand);
    }

}
