import {injectable, inject} from "inversify";
import {ProjectConfiguration} from '../Entity/ProjectConfiguration'
import {Config} from '../Interface/Config'
import {CreateProjectAnswers} from '../Interface/CreateProjectAnswers'
import * as path from 'path'

@injectable()
export class PathBuilder {
    private config: Config

    public constructor(
        @inject(Symbol.for("Config")) config: Config
    ) {
        this.config = config
    }

    public projectFromConfiguration(projectConfiguration: ProjectConfiguration): string {
        return `${this.resolveBaseDirectory()}/${projectConfiguration.client}/${projectConfiguration.name}`
    }

    public projectFromAnswers(answers: CreateProjectAnswers): string {
        return `${this.resolveBaseDirectory()}/${answers.client}/${answers.projectName}`
    }

    private resolveBaseDirectory(): string {
        return path.resolve(this.config.baseDirectory)
    }
}
