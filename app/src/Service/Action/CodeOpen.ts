import { inject, injectable } from 'inversify'
import {ProjectConfigurationRepository} from './../../Repository/ProjectConfigurationRepository'
import {ProcessExecutor} from './../ProcessExecutor'
import {Project} from './../../Entity/Project'
import {ProjectConfiguration} from './../../Entity/ProjectConfiguration'

@injectable()
export class CodeOpen {
    private projectConfigurationRepository: ProjectConfigurationRepository
    private processExecutor: ProcessExecutor

    get title(): string {
        return 'Open the code'
    }

    public constructor (
        @inject(Symbol.for("ProjectConfigurationRepository")) projectConfigurationRepository: ProjectConfigurationRepository,
        @inject(Symbol.for("ProcessExecutor")) processExecutor: ProcessExecutor
    ) {
        this.projectConfigurationRepository = projectConfigurationRepository
        this.processExecutor = processExecutor
    }

    public async run(project: Project) {
        let configuration: ProjectConfiguration
        try {
            configuration = await this.projectConfigurationRepository.findByProject(project)
        } catch (e) {
            console.error(e)
            return
        }

        const commandNames: string[] = configuration.getCommandsByTag('Open project')
        if (!commandNames.length) {
            console.log("No command found")
            return
        }

        try {
            await this.processExecutor.execute(commandNames, project)
        } catch (e) {
            console.error(e)
            return
        }

    }
}
