import {injectable} from 'inversify'
import {ProjectConfiguration} from './../Entity/ProjectConfiguration'
import {Project} from './../Entity/Project'
import {ProjectTypeChar} from './../Enum/ProjectTypeChar'

@injectable()
export class ProjectConfigurationMapper {
    public fromProjectConfigurationToProject(projectConfiguration: ProjectConfiguration): Project {
        let project = new Project()

        project.client = projectConfiguration.client
        project.name = projectConfiguration.name
        project.path = projectConfiguration.path
        project.typeChar = ProjectTypeChar[<any>projectConfiguration.type]

        return project
    }

    fromProjectConfigurationsToProjects(projectConfigurations: ProjectConfiguration[]): Project[] {
        var projects: Project[] = []
        var me = this
        projectConfigurations.forEach((projectConfiguration) => {
            projects.push(me.fromProjectConfigurationToProject(projectConfiguration))
        })

        return projects
    }
}
