import { Project } from './../Entity/Project'

export class ProjectSerializer {
    static deserialize(line: string): Project {
        let project = new Project()

        let [ typeChar, name, client, path ] = line.split(' - ')
        project.typeChar = typeChar
        project.name = name
        project.path = path
        project.client = client

        return project
    }

    // TODO Replace the full path by the folder name in project Code
    // directory.
    // The full path to the repository can be infered from the client
    // and the name of the project
    static serialize(project: Project): string {
        return `${project.typeChar} - ${project.name} - ${project.client} - ${project.path}`
    }
}
