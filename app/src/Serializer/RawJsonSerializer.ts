import {Deserialize,Serialize,ISerializable} from 'cerialize'

export class RawJsonSerializer {
    public static JSON_SPACE = 2

    public static serialize(from: any, to: object) : string {
        return JSON.stringify(Serialize(from, to), null, RawJsonSerializer.JSON_SPACE)
    }

    public static deserialize(from: string, to: any) : any {
        return Deserialize(JSON.parse(from), to)
    }
}

