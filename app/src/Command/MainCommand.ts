import { inject, injectable } from 'inversify'
import {ProjectRepository} from './../Repository/ProjectRepository'
import {Project} from './../Entity/Project'
import {RofiMenu} from './../View/Rofi/RofiMenu'
import {CodeOpen} from './../Service/Action/CodeOpen'

@injectable()
export class MainCommand {
    private projectRepository: ProjectRepository
    private codeOpen: CodeOpen

    public constructor (
        @inject(Symbol.for("ProjectRepository")) projectRepository: ProjectRepository,
        @inject(Symbol.for("CodeOpen")) codeOpen: CodeOpen
    ) {

        this.projectRepository = projectRepository
        this.codeOpen = codeOpen
    }

    public async execute() {
        let projects: Project[];
        const me = this
        try {
            projects = await this.projectRepository.findAll()
        } catch (e) {
            console.error(e)
            return;
        }

        let view = new RofiMenu(projects);
        view.on({
            'Alt+o': this.codeOpen
        })

        try {
            view.render();
        } catch (e) {
            console.error(e);
            return;
        }

    }
}
