import {injectable, inject} from 'inversify'
import { Answers, prompt, Question} from 'inquirer';
import {CreateProjectAction} from './../Enum/CreateProjectAction'
import {CreateProjectAnswers} from './../Interface/CreateProjectAnswers'
import {CommonOptions} from './../Interface/CommonOptions'
import {ProjectConfig} from './../Interface/ProjectConfig'
import {Config} from './../Interface/Config'
import {FileSystem} from './../Service/FileSystem'
import {ProjectConfiguration} from './../Entity/ProjectConfiguration'
import {ProjectConfigurationRepository} from './../Repository/ProjectConfigurationRepository'
import {PathBuilder} from '../Service/PathBuilder'
import {PassService} from '../Service/PassService'
import {GitService} from '../Service/GitService'
import * as ejs from 'ejs'

@injectable()
export class CreateProjectCommand {
    private projectConfig: ProjectConfig
    private config: Config
    private fileSystem: FileSystem
    private projectConfigurationRepository: ProjectConfigurationRepository
    private pathBuilder: PathBuilder
    private passService: PassService
    private gitService: GitService

    public constructor(
        @inject(Symbol.for("ProjectConfig")) projectConfig: ProjectConfig,
        @inject(Symbol.for("Config")) config: Config,
        @inject(Symbol.for("FileSystem")) fileSystem: FileSystem,
        @inject(Symbol.for("ProjectConfigurationRepository")) projectConfigurationRepository: ProjectConfigurationRepository,
        @inject(Symbol.for("PathBuilder")) pathBuilder: PathBuilder,
        @inject(Symbol.for("PassService")) passService: PassService,
        @inject(Symbol.for("GitService")) gitService: GitService
    ) {
        this.projectConfig = projectConfig
        this.config = config
        this.fileSystem = fileSystem
        this.projectConfigurationRepository = projectConfigurationRepository
        this.pathBuilder = pathBuilder
        this.passService = passService
        this.gitService = gitService
    }

    public async execute() {
        let answers: CreateProjectAnswers
        try {
            answers = await this.prompting()
        } catch (e) {
            console.error(e)
            return process.exit(1)
        }

        try {
            await this.writing(answers)
        } catch (e) {
            console.error(e)
            return process.exit(1)
        }

        try {
            await this.installing(answers)
        } catch (e) {
            console.error(e)
            return process.exit(1)
        }
    }

    private async prompting() {
        const that = this;

        return await prompt([
            {
                type: "input",
                name: "projectName",
                message: "Your project name"
            },
            {
                type: "input",
                name: "client",
                message: "Who is this project for ?",
                default: "CaptainQuirk"
            },
            {
                type: "input",
                name: "type",
                message: "What type of project is this ?"
            },
            {
                type: "list",
                name: "action",
                message: "Select action",
                choices: [CreateProjectAction.CREATE, CreateProjectAction.IMPORT],
                default: CreateProjectAction.CREATE
            },
            {
                type: "input",
                name: "repository",
                message: "Type git repository",
                when: function(answers) {
                    return answers.action === CreateProjectAction.IMPORT
                }
            },
            {
                type: "confirm",
                name: "getPassword",
                message: "Do you need to copy a password in the clipboard ?"
            },
            {
                type: "list",
                name: "pass",
                message: "Select pass",
                choices: function() {
                    return that.passService.getAll()
                },
                when: function(answers) {
                    return answers.getPassword;
                }
            }
        ]);
    }

    public async writing(answers: CreateProjectAnswers) {
        const projectConfiguration = new ProjectConfiguration(
            answers.projectName,
            answers.client,
            answers.type
        );

        await this._writeTmuxinatorFile(answers)
        await this.projectConfigurationRepository.save(projectConfiguration)
    }

    public async installing(answers: CreateProjectAnswers) {
        const projectPath = this.pathBuilder.projectFromAnswers(answers)
        switch (answers.action) {
            case CreateProjectAction.CREATE:
                await this.fileSystem.createDirIfNotExist(`${projectPath}/Code`)
                break

            case CreateProjectAction.IMPORT:
                if (answers.pass) {
                    this.passService.copyToClipboard(answers.pass)
                }

                this.gitService.clone(
                    answers.repository,
                    this.pathBuilder.projectFromAnswers(answers)
                )

                break;
        }

        await this.fileSystem.symlink(`${projectPath}/.config/koorc.json`, `${projectPath}/Code/koorc.json`);
        await this.fileSystem.symlink(`${projectPath}/.config/tmuxinator.open.yml`, `${projectPath}/Code/tmuxinator.open.yml`);
    }

    private async _writeTmuxinatorFile(answers: CreateProjectAnswers) {
        const directory = `${this.config.baseDirectory}/${answers.client}/${answers.projectName}/.config`
        try {
            await this._writeTpl(
                `${this.projectConfig.ROOT_DIR}/templates/tmuxinator.open.yml.ejs`,
                `${directory}/tmuxinator.open.yml`,
                answers
            )
        } catch (e) {
            console.trace(e)
            return
        }
    }

    private async _writeTpl(template: string, path: string, answers: CreateProjectAnswers) {
        let content: string
        try {
            content = await this._renderTpl(template, path, answers)
        } catch (e) {
            console.log(e)
            return
        }

        try {
            await this._write(path, content)
        } catch (e) {
            console.log(e)
            return
        }
    }

    private async _write(filePath: string, content: string) {

        const fileExists = await this.fileSystem.fileExists(filePath)
        let writeFile: boolean = true
        if (fileExists) {
            writeFile = await prompt([
                {
                    type: "confirm",
                    name: "overwriteFile",
                    message: `File ${filePath} already exists. Do you wish to overwrite ?`
                }
            ])
        }

        await this.fileSystem.write(filePath, content)
    }

    private async _renderTpl(template: string, path: string, answers: CreateProjectAnswers): Promise<string>{

        return new Promise((resolve: Function, reject: Function) => {
            const html = ejs
                .renderFile(template, answers)
                  .then(output => {
                      resolve(output)
                  })
                  .catch(e => {
                      reject(e)
                  })
        })
    }
}
