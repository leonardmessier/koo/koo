import { inject, injectable } from 'inversify'
import {ProjectConfigurationRepository} from './../Repository/ProjectConfigurationRepository'
import {ProjectRepository} from './../Repository/ProjectRepository'
import {ProjectConfiguration} from './../Entity/ProjectConfiguration'
import {ProjectConfigurationMapper} from './../Mapper/ProjectConfigurationMapper'

@injectable()
export class IndexCommand {
    private projectConfigurationRepository: ProjectConfigurationRepository
    private projectRepository: ProjectRepository
    private projectConfigurationMapper: ProjectConfigurationMapper

    public constructor (
        @inject(Symbol.for("ProjectConfigurationRepository")) projectConfigurationRepository: ProjectConfigurationRepository,
        @inject(Symbol.for("ProjectRepository")) projectRepository: ProjectRepository,
        @inject(Symbol.for("ProjectConfigurationMapper")) projectConfigurationMapper: ProjectConfigurationMapper
    ) {
        this.projectConfigurationRepository = projectConfigurationRepository
        this.projectRepository = projectRepository
        this.projectConfigurationMapper = projectConfigurationMapper
    }

    public async execute() {
        let configurations: any
        try {
            configurations = await this.projectConfigurationRepository.findAll()
        } catch (e) {
            console.error(e)
            return;
        }

        let projects = this.projectConfigurationMapper.fromProjectConfigurationsToProjects(configurations)

        try {
            this.projectRepository.saveAll(projects)
        } catch (e) {
            console.log(e)
            process.exit(1)
            return
        }

        console.info("Index written")
    }
}
