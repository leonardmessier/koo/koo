import {ContainerBuilder} from './../config/ContainerBuilder'
import {MainCommand} from './../src/Command/MainCommand'
import {CommonOptions} from './../src/Interface/CommonOptions'
import program from 'commander'

program
    .option('-c, --config <config>', 'Config file to use')
    .action(async (command) => {
        const programOptions = command.opts()
        const options: CommonOptions = {
            config: programOptions.config
        }

        const mainCommand: MainCommand = ContainerBuilder
            .getContainer(options)
            .get<MainCommand>(Symbol.for("MainCommand"))
        await mainCommand.execute()
    })

program.parse(process.argv)
