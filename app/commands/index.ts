import {ContainerBuilder} from './../config/ContainerBuilder'
import {IndexCommand} from './../src/Command/IndexCommand'
import {CommonOptions} from './../src/Interface/CommonOptions'
import program from 'commander'

program
    .option('-c, --config', 'Config file to use')
    .action(async (command) => {
        const programOptions = command.opts()
        const options: CommonOptions = {
            config: programOptions.config
        }

        const indexCommand: IndexCommand = ContainerBuilder
            .getContainer(options)
            .get<IndexCommand>(Symbol.for("IndexCommand"))

        await indexCommand.execute()
    })

program.parse(process.argv)
