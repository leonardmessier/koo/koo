import './polyfills'
import program from 'commander'

import * as utils from './lib/utils'
program
  .version('1.0.0')
  .description('Project Management')

program
  .command('main', 'Main koo action', {
      executableFile: './commands/main',
      isDefault: true,
      noHelp: true
  })

program
  .command('index', 'Index projects', { executableFile: './commands/index' })
  .alias('i')

program
  .command('create-project', 'Create project', { executableFile: './commands/create-project' })

program.parse(process.argv);
