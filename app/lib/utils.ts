import * as path from 'path'
import * as fs from 'fs'

const readKoorcFile = function(file: string) {
    const fullPath = path.dirname(file);
    return new Promise(function(resolve, reject) {
        fs.readFile(file, 'utf8', function(err, data) {
            if (err) {
                return reject(err);
            }

            const koorc = JSON.parse(data);
            return resolve({
                path: fullPath,
                config: koorc
            });
        });
    });
}

export {
    readKoorcFile as readKoorcFile
}
