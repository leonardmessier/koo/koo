import "reflect-metadata";
import {Container} from "inversify";
import {ProjectRepository} from './../src/Repository/ProjectRepository'
import {ProjectConfigurationRepository} from './../src/Repository/ProjectConfigurationRepository'
import {ProcessExecutor} from './../src/Service/ProcessExecutor'
import {MainCommand} from './../src/Command/MainCommand'
import {IndexCommand} from './../src/Command/IndexCommand'
import {CreateProjectCommand} from './../src/Command/CreateProjectCommand'
import {CommonOptions} from './../src/Interface/CommonOptions'
import {FileSystem} from './../src/Service/FileSystem'
import {Config} from './../src/Interface/Config'
import {ProjectConfig} from './../src/Interface/ProjectConfig'
import {ProjectConfigurationMapper} from './../src/Mapper/ProjectConfigurationMapper'
import {MissingConfigFileError} from './../src/Error/MissingConfigFileError'
import {PathBuilder} from './../src/Service/PathBuilder'
import {CodeOpen} from './../src/Service/Action/CodeOpen'
import {PassService} from './../src/Service/PassService'
import {GitService} from './../src/Service/GitService'
import {PassRepository} from './../src/Repository/PassRepository'
import * as fs from 'fs'
import * as path from 'path'
import * as _ from 'lodash'

export class ContainerBuilder {
  public static getContainer(options: { [key: string]: any }): Container {
        let container = new Container()

        container.bind<ProjectRepository>(Symbol.for("ProjectRepository")).to(ProjectRepository).inSingletonScope();
        container.bind<ProjectConfigurationRepository>(Symbol.for("ProjectConfigurationRepository")).to(ProjectConfigurationRepository).inSingletonScope();
        container.bind<ProjectConfigurationMapper>(Symbol.for("ProjectConfigurationMapper")).to(ProjectConfigurationMapper).inSingletonScope();
        container.bind<ProcessExecutor>(Symbol.for("ProcessExecutor")).to(ProcessExecutor).inSingletonScope();
        container.bind<MainCommand>(Symbol.for("MainCommand")).to(MainCommand).inSingletonScope();
        container.bind<IndexCommand>(Symbol.for("IndexCommand")).to(IndexCommand).inSingletonScope();
        container.bind<CreateProjectCommand>(Symbol.for("CreateProjectCommand")).to(CreateProjectCommand).inSingletonScope();
        container.bind<FileSystem>(Symbol.for("FileSystem")).to(FileSystem).inSingletonScope();
        container.bind<PathBuilder>(Symbol.for("PathBuilder")).to(PathBuilder).inSingletonScope();
        container.bind<Config>(Symbol.for("Config")).toConstantValue(this.getConfig(options))
        container.bind<ProjectConfig>(Symbol.for("ProjectConfig")).toConstantValue({
            ROOT_DIR: path.dirname(__dirname)
        });
        container.bind<CodeOpen>(Symbol.for("CodeOpen")).to(CodeOpen).inSingletonScope();
        container.bind<PassService>(Symbol.for("PassService")).to(PassService).inSingletonScope();
        container.bind<PassRepository>(Symbol.for("PassRepository")).to(PassRepository).inSingletonScope();
        container.bind<GitService>(Symbol.for("GitService")).to(GitService).inSingletonScope();

        return container
    }

    private static getConfig(options: { [key: string]: any }): Config {
        const env = process.env
        let config: Config
        let defaultConfig: Config = {
            directories: [`${env.HOME}/Projects/**`],
            baseDirectory: `${env.HOME}/Projects`,
            index: `${env.HOME}/.cache/koo/index`
        }

        if (options.config) {
            config = this.readConfigFromFile(options.config)
        } else {
            const userConfig: string = `${env.XDG_CONFIG_HOME}/koo/config`
            if (fs.existsSync(userConfig)) {
                config = _.merge(defaultConfig, this.readConfigFromFile(userConfig))
            } else {
                config = defaultConfig
            }
        }

        return config;
    }

    private static readConfigFromFile(configPath: string): Config {
        if (!fs.existsSync(configPath)) {
            throw new MissingConfigFileError(`No config file found at configPath « ${configPath} »`);
        }

        const configFile = fs.readFileSync(configPath, { encoding: 'UTF-8' })

        return JSON.parse(configFile)
    }
}

